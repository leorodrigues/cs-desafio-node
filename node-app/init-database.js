
const MONGO_SERVER = process.env.MONGO_SERVER || 'localhost';
const MONGO_PORT = parseInt(process.env.MONGO_PORT || 27017);

const mongodb = require('mongodb');
const winston = require('winston');

function abrirConexao() {
    return new Promise((resolve, reject) => {
        let client = mongodb.MongoClient;
        let url = `mongodb://${MONGO_SERVER}:${MONGO_PORT}/`;
        winston.info(`Abrindo conexão com '${url}'.`);
        client.connect(url, function(erro, conexao) {
            if (erro) winston.error(erro.message);
            if (conexao) {
                winston.info('Conexão concluída.')
                return resolve({conexao, db: conexao.db('db_usuarios')}); 
            }
            reject(new Error('A conexão não pôde ser aberta.'));
        });
    });
}

function removerColecaoAntiga({conexao, db}) {
    return new Promise((resolve, reject) => {
        winston.info('Buscando coleções existentes para remoção.')
        db.listCollections({name: 'usuarios'}).next((erro, info) => {
            if (erro) winston.warn(erro.message);
            if (!info) {
                winston.info('A coleção não existe. Não é necessário remover.');
                return resolve({conexao, db});
            }
            winston.info('Uma coleção encontrada. Removendo...');
            db.collection('usuarios').drop((erro, ok) => {
                if (erro) winston.warn(erro.message);
                if (ok) {
                    winston.info('Coleção removida.');
                    return resolve({conexao, db});
                }
                reject(new Error('A coleção não pôde ser removida.'));
            });
        });
    });
}

function criarColecao({conexao, db}) {
    return new Promise((resolve, reject) => {
        winston.info('Criando coleção de usuários.');
        db.createCollection('usuarios', function(erro, res) {
            if (erro) {
                winston.warn(erro.message);
                return reject(new Error('A coleção não pôde ser criada.'));
            }
            winston.info('Coleção criada.');
            resolve({conexao, db});
        });
    });
}

function tratarErro(erro) {
    winston.error(erro.message);
    process.exit(1);
}

abrirConexao().then(state => {
    return removerColecaoAntiga(state).then(state => {
        return criarColecao(state).then(({conexao}) => {
            conexao.close();
        }, tratarErro);
    }, tratarErro);
}, tratarErro);