const proxyquire = require('proxyquire');
const chai = require('chai');
const sinon = require('sinon');

const expect = chai.expect;

chai.use(require('sinon-chai'));

const {ErroNegocio} = require('../lib/model/erros-negocio');

const {ErroCredencialInvalida} = require('../lib/model/erros-negocio');

describe('Usuario', () => {
    describe('#entrar', () => {
        let sandbox = sinon.createSandbox();
        let buscarUsuarioPorEmailIvk = sandbox.stub();

        let usuario = proxyquire('../lib/model/usuario', {
            '../service/database': {
                buscarUsuarioPorEmail: buscarUsuarioPorEmailIvk
            }
        });

        afterEach(() => {
            sandbox.reset();
        }); 

        it('Should reject with ErroCredencialInvalida if usuario is not found.', done => {
            let resolve = () => done(new Error('Não deveria resolver'));
            buscarUsuarioPorEmailIvk.returns(Promise.resolve(null));
            usuario.entrar({email: 'address'}).then(resolve, erro => {
                expect(erro).to.be.instanceOf(ErroCredencialInvalida);
                expect(erro.message).to.be.equals('Usuário e/ou senha inválidos.');
                done();
            });
        });

        it('Should reject with ErroCredencialInvalida if senha does not match.', done => {
            let resolve = () => done(new Error('Não deveria resolver'));
            buscarUsuarioPorEmailIvk.returns(Promise.resolve({senha: 'x'}));
            usuario.entrar({email: 'address', senha: 'y'}).then(resolve, erro => {
                expect(erro).to.be.instanceOf(ErroCredencialInvalida);
                expect(erro.message).to.be.equals('Usuário e/ou senha inválidos.');
                done();
            });
        });

        it('Should reject if database rejects the query', done => {
            let resolve = () => done(new Error('Não deveria resolver'));
            buscarUsuarioPorEmailIvk.returns(Promise.reject(
                new Error('thrown on purpose')));
            usuario.entrar({email: 'address'}).then(resolve, erro => {
                expect(erro).to.be.instanceOf(Error);
                expect(erro.message).to.be.equals('thrown on purpose');
                done();
            });
        });

        it('Should resolve with usuario from database if credentials are ok.', done => {
            let usuarioDaBase = {senha: 'x'};
            let resolve = usr => {
                expect(usr).to.be.equals(usuarioDaBase);
                done();
            };
            buscarUsuarioPorEmailIvk.returns(Promise.resolve(usuarioDaBase));
            usuario.entrar({email: 'address', senha: 'x'}).then(resolve, done);
        });
    });

    describe('#novo', () => {
        let sandbox = sinon.createSandbox();
        let inserirInvocation = sandbox.stub();
        let existeUsuarioComEmailInvocation = sandbox.stub();

        let usuario = proxyquire('../lib/model/usuario', {
            'moment': () => ({valueOf: () => 112358}),
            'guid': {raw: () => 'xpto'},
            '../service/database': {
                inserir: function(input) {
                    return inserirInvocation(input);
                },
                existeUsuarioComEmail: function(email) {
                    return existeUsuarioComEmailInvocation(email);
                }
            }
        });

        let input = {
            nome: 'Joao da Silva',
            email: 'jsilva@company.com',
            senha: 'abcd1234',
            telefones: [
                {numero: '123456789', ddd: '21'}
            ]
        };

        beforeEach(() => {
            inserirInvocation = sandbox.stub();
            existeUsuarioComEmailInvocation = sandbox.stub();
        });

        afterEach(() => {
            sandbox.reset();
        });

        it('Should resolve with correct fields', done => {
            let handleSuccess = usuario => {
                expect(existeUsuarioComEmailInvocation)
                    .to.be.calledOnce
                    .and.to.be.calledWith('jsilva@company.com');
                expect(usuario.nome).to.be.equals('Joao da Silva');
                expect(usuario.email).to.be.equals('jsilva@company.com');
                expect(usuario.senha).to.be.equals('abcd1234');
                expect(usuario.data_criacao).to.be.equals(112358);
                expect(usuario.data_atualizacao).to.be.equals(112358);
                expect(usuario.ultimo_login).to.be.equals(112358);
                expect(usuario.token).to.be.equals('xpto');
                expect(usuario.id).to.be.equals(132134);
                expect(usuario.telefones).to.be.deep.equals([
                    {numero: '123456789', ddd: '21'}
                ]);
                done();
            };
            inserirInvocation.returns(Promise.resolve(132134));
            existeUsuarioComEmailInvocation.returns(Promise.resolve(false));
            usuario.novo(input)
                .then(handleSuccess, done)
                .catch(done);
        });

        it('Should reject with ErroNegocio if "email" is found', done => {
            let handleRejection = error => {
                expect(error).to.be.instanceOf(ErroNegocio);
                done();
            };
            let handleResolution = () => {
                done(new Error('execution should never reach this point'));
            };
            existeUsuarioComEmailInvocation.returns(Promise.resolve(true));
            usuario.novo(input)
                .then(handleResolution, handleRejection)
                .catch(done);
        });

        it('Should reject if email verification is rejected', done => {
            let handleRejection = error => {
                expect(error).to.be.equals('error');
                done();
            };
            let handleResolution = () => {
                done(new Error('execution should never reach this point'));
            };
            existeUsuarioComEmailInvocation.returns(Promise.reject('error'));
            usuario.novo(input)
                .then(handleResolution, handleRejection)
                .catch(done);
        });
        
        it('Should reject if insertion is rejected', done => {
            let handleRejection = error => {
                expect(error).to.be.equals('error');
                done();
            };
            let handleResolution = () => {
                done(new Error('execution should never reach this point'));
            };
            inserirInvocation.returns(Promise.reject('error'));
            existeUsuarioComEmailInvocation.returns(Promise.resolve(false));
            usuario.novo(input)
                .then(handleResolution, handleRejection)
                .catch(done);
        });
    });
});