const proxyquire = require('proxyquire');
const chai = require('chai');
const sinon = require('sinon');

const expect = chai.expect;

chai.use(require('sinon-chai'));

let sandbox = sinon.createSandbox();
let isAfterInvocation = sandbox.stub();
let addInvocation = sandbox.stub();

let momentMock = {
    isAfter: function(moment) {
        return isAfterInvocation(moment);
    },
    add: function(amount, dimension) {
        return addInvocation(amount, dimension);
    }
};

let autenticacao = proxyquire('../lib/service/autenticacao', {
    moment: function() { return momentMock; }
});

describe('Autenticacao', () => {
    describe('#iniciarSessao', () =>{
        describe('W/o environment config', () => {
            let autenticacaoAlt = null;

            beforeEach(() => {
                autenticacaoAlt = proxyquire('../lib/service/autenticacao', {
                    moment: function() { return momentMock; }
                });
            });

            afterEach(() => {
                sandbox.reset();
            });

            it('Should call "moment#add" with default value (180)', () => {
                return autenticacaoAlt.iniciarSessao('token').then(() => {
                    expect(addInvocation)
                        .to.be.called
                        .and.to.be.calledWith(180, 'seconds');
                });
            });
        });

        describe('With environment config', () => {
            let autenticacaoAlt = null;

            beforeEach(() => {
                process.env.DURACAO_SESSAO_EM_SEGUNDOS = 13;
                autenticacaoAlt = proxyquire('../lib/service/autenticacao', {
                    moment: function() { return momentMock; }
                });
            });

            afterEach(() => {
                delete process.env.DURACAO_SESSAO_EM_SEGUNDOS;
                sandbox.reset();
            });

            it('Should call "moment#add" with config value', () => {
                return autenticacaoAlt.iniciarSessao('token').then(() => {
                    expect(addInvocation).to.be.called;
                    expect(addInvocation)
                        .and.to.be.calledWith(13, 'seconds');
                });
            });
        });
    });

    describe('#sessaoValida', () => {

        afterEach(() => sandbox.reset());

        it('Should resolve with false if token is undefined', () => {
            return autenticacao.sessaoValida().then(valida =>
                expect(valida).to.be.false);
        });

        it('Should resolve with false if token is null', () => {
            return autenticacao.sessaoValida(null).then(valida =>
                expect(valida).to.be.false);
        });

        it('Should resolve with false if token is unknown', () => {
            return autenticacao.sessaoValida('x').then(valida => {
                expect(valida).to.be.false;
                expect(isAfterInvocation).to.not.be.called;
            });
        });

        it('Should resolve with false if token is expired', () => {
            isAfterInvocation.returns(true);
            addInvocation.returns(11);
            return autenticacao.iniciarSessao('some-token').then(() => {
                return autenticacao.sessaoValida('some-token').then(valida => {
                    expect(valida).to.be.false;
                    expect(addInvocation)
                        .to.be.called
                        .and.to.be.calledWith(180, 'seconds');
                    expect(isAfterInvocation)
                        .to.be.called
                        .and.to.be.calledWith(11);
                });
            });
        });

        it('Should resolve with true if token is ok', () => {
            isAfterInvocation.returns(false);
            addInvocation.returns(7);
            return autenticacao.iniciarSessao('some-token').then(() => {
                return autenticacao.sessaoValida('some-token').then(valida => {
                    expect(valida).to.be.true;
                    expect(addInvocation)
                        .to.be.called
                        .and.to.be.calledWith(180, 'seconds');
                    expect(isAfterInvocation)
                        .to.be.called
                        .and.to.be.calledWith(7);
                });
            });
        });
    });

    describe('#obterToken', () => {
        
        it('Should return null if parameter is undefined', () => {
            expect(autenticacao.obterToken()).to.be.null;
        });
        it('Should return null if parameter is null', () => {
            expect(autenticacao.obterToken(null)).to.be.null;
        });
        it('Should return null if "Authentication" header is missing', () => {
            expect(autenticacao.obterToken({})).to.be.null;
        });
        it('Should return null if "Authentication" header is malformed', () => {
            expect(autenticacao.obterToken({'authorization': 'x'})).to.be.null;
        });
        it('Should return token if "Authentication" header is ok', () => {
            let headers = {'authorization': 'Bearer x'};
            expect(autenticacao.obterToken(headers)).to.be.equals('x');
        });
    });
});