const proxyquire = require('proxyquire');
const chai = require('chai');
const sinon = require('sinon');

const expect = chai.expect;

chai.use(require('sinon-chai'));

const {
    UnauthorizedError,
    ConflictError,
    BadRequestError,
    InternalServerError
} = require('../lib/routing/http-errors');

const {ErroNegocio} = require('../lib/model/erros-negocio');

describe('Routes', () => {
    describe('GET /usuario/:id', () => {
        let sandbox = sinon.createSandbox();
        let sessaoValidaIvk = sandbox.stub();
        let buscarUsuarioPorIdIvk = sandbox.stub();
        let obterTokenIvk = sandbox.stub();
        let statusIvk = sandbox.stub();
        let jsonIvk = sandbox.stub();

        let router = proxyquire('../lib/routing/routes.js', {
            '../service/autenticacao': {
                sessaoValida: sessaoValidaIvk,
                obterToken: obterTokenIvk
            },
            '../service/database': { buscarUsuarioPorId: buscarUsuarioPorIdIvk }
        });

        afterEach(() => {
            sandbox.reset();
        });

        it('Should forward "UnauthorizedError" if token is missing.', done => {
            let headers = 'some headers';
            let req = {method: 'GET', url: '/usuario/x', headers};
            obterTokenIvk.returns(null);
            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(UnauthorizedError);
                expect(erro.message).to.be.equals('Não autorizado.');
                expect(obterTokenIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith(headers);
                done();
            });
        });

        it('Should forward "UnauthorizedError" if tokens don\'t match.', done => {
            let headers = 'some headers';
            let req = {method: 'GET', url: '/usuario/x', headers};
            obterTokenIvk.returns('a');
            buscarUsuarioPorIdIvk.returns(Promise.resolve({token: 'b'}));
            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(UnauthorizedError);
                expect(erro.message).to.be.equals('Não autorizado.');
                expect(obterTokenIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith(headers);
                expect(buscarUsuarioPorIdIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith('x');
                done();
            });
        });

        it('Should forward "UnauthorizedError" if session is invalid.', done => {
            let headers = 'some headers';
            let req = {method: 'GET', url: '/usuario/x', headers};
            obterTokenIvk.returns('a');
            buscarUsuarioPorIdIvk.returns(Promise.resolve({token: 'a'}));
            sessaoValidaIvk.returns(Promise.resolve(false));
            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(UnauthorizedError);
                expect(erro.message).to.be.equals('Sessão inválida.');
                expect(obterTokenIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith(headers);
                expect(buscarUsuarioPorIdIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith('x');
                expect(sessaoValidaIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith('a');
                done();
            });
        });

        it('Should forward "InternalServerError" if exception is thrown.', done => {
            let headers = 'some headers';
            let req = {method: 'GET', url: '/usuario/x', headers};
            obterTokenIvk.returns('a');
            buscarUsuarioPorIdIvk.returns(Promise.resolve({
                get token() { throw new Error('thrown on purpose'); }
            }));
            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(InternalServerError);
                expect(erro.message).to.be.equals('Erro interno do servidor.');
                expect(obterTokenIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith(headers);
                expect(buscarUsuarioPorIdIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith('x');
                done();
            });
        });

        it('Should forward ahead to the next middleware if user is not found.', done => {
            let headers = 'some headers';
            let req = {method: 'GET', url: '/usuario/x', headers};
            obterTokenIvk.returns('a');
            buscarUsuarioPorIdIvk.returns(Promise.resolve(null));
            router(req, {}, erro => {
                expect(erro).to.be.undefined;
                expect(obterTokenIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith(headers);
                expect(buscarUsuarioPorIdIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith('x');
                done();
            });
        });

        it('Should forward InternalServerError when database rejects.', done => {
            let headers = 'some headers';
            let req = {method: 'GET', url: '/usuario/x', headers};
            obterTokenIvk.returns('a');
            buscarUsuarioPorIdIvk.returns(
                Promise.reject(new Error('thrown on purpose')));
            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(InternalServerError);
                expect(erro.message).to.be.equals('Erro interno do servidor.');
                expect(obterTokenIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith(headers);
                expect(buscarUsuarioPorIdIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith('x');
                done();
            });
        });

        it('Should forward InternalServerError when session validation rejects.', done => {
            let headers = 'some headers';
            let req = {method: 'GET', url: '/usuario/x', headers};
            obterTokenIvk.returns('a');
            buscarUsuarioPorIdIvk.returns(Promise.resolve({token: 'a'}));
            sessaoValidaIvk.returns(
                Promise.reject(new Error('thrown on purpose')));
            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(InternalServerError);
                expect(erro.message).to.be.equals('Erro interno do servidor.');
                expect(obterTokenIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith(headers);
                expect(buscarUsuarioPorIdIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith('x');
                expect(sessaoValidaIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith('a');
                done();
            });
        });

        it('Should respond with "usuario" from database.', done => {
            let headers = 'some headers';
            let req = {method: 'GET', url: '/usuario/x', headers};
            let usuario = {token: 'a'};
            obterTokenIvk.returns('a');
            buscarUsuarioPorIdIvk.returns(Promise.resolve(usuario));
            sessaoValidaIvk.returns(Promise.resolve(true));

            let res = {status: statusIvk, json: jsonIvk, end: () => {
                expect(statusIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith(200);
                expect(jsonIvk)
                    .to.be.calledOnce
                    .and.to.be.calledWith(usuario);
                done();
            }};

            router(req, res, done);
        });
    });

    describe('POST /usuario', () => {
        describe('In case of failure', () => {
            let sandbox = sinon.createSandbox();
            let novoInvocation = null;

            let router = proxyquire('../lib/routing/routes.js', {
                '../model/usuario': {
                    novo: function(input) {
                        return novoInvocation(input);
                    }
                }
            });

            beforeEach(() => {
                novoInvocation = sandbox.stub();
            });

            afterEach(() => {
                sandbox.reset();
            });

            it('Should forward BadRequestError for wrong content type.', done => {
                let request = {
                    method: 'POST',
                    url: '/usuario',
                    headers: {
                        'content-type': 'text/plain'
                    }
                };

                router(request, {}, error => {
                    expect(error)
                        .to.be.instanceOf(BadRequestError);

                    expect(error.message)
                        .to.be.equal('Tipo de conteúdo inválido.');

                    done();
                });
            });

            it('Should forward BadRequestError for malformed json.', done => {
                let request = {
                    method: 'POST',
                    url: '/usuario',
                    headers: {
                        'content-type': 'application/json'
                    },
                    get body() {
                        throw new Error('thrown on purpose');
                    }
                };

                router(request, {}, error => {
                    expect(error)
                        .to.be.instanceOf(BadRequestError);
                        
                    expect(error.message)
                        .to.be.equal('Json inválido.');
                    
                    done();
                });
            });

            it('Should forward InternalServerError in case of rejection.', done => {
                novoInvocation.returns(new Promise((res, rej) => {
                    rej(new Error('thrown on purpose'));
                }));

                let body = {};
                let request = {
                    method: 'POST',
                    url: '/usuario',
                    headers: {
                        'content-type': 'application/json'
                    },
                    body
                };

                router(request, {}, error => {
                    expect(error)
                        .to.be.instanceOf(InternalServerError);
                        
                    expect(error.message)
                        .to.be.equal('Erro interno do servidor.');
                    
                    done();
                });
            });
        });

        describe('In case of success', () => {
            let body = {};
            let usuario = {};
            let sandbox = sinon.createSandbox();
            let nextInvocation = sandbox.stub();
            let responseJsonInvocation = sandbox.stub();
            let responseEndInvocation = sandbox.stub();
            let responseStatusInvocation = sandbox.stub();
            let novoInvocation = sandbox.stub();
            novoInvocation.returns(new Promise(resolve => resolve(usuario)));

            let router = proxyquire('../lib/routing/routes.js', {
                '../model/usuario': {novo: novoInvocation}
            });

            let request = {
                method: 'POST',
                url: '/usuario',
                headers: {'content-type': 'application/json'},
                body
            };

            let response = {
                status: responseStatusInvocation,
                json: responseJsonInvocation,
                end: responseEndInvocation
            };

            router(request, response, nextInvocation);

            it('Should include new "usuario" in response body', done => {
                expect(novoInvocation)
                    .to.be.calledOnce
                    .and.to.be.calledWith(body);

                expect(responseJsonInvocation)
                    .to.be.calledOnce
                    .and.to.be.calledWith(usuario);

                done();
            });

            it('Should return HTTP status code 200', done => {
                expect(responseStatusInvocation)
                    .to.be.calledOnce
                    .and.to.be.calledWith(200);

                expect(responseEndInvocation)
                    .to.be.calledOnce;

                done();
            });

            it('Should not forward the request', done => {
                expect(nextInvocation).to.not.be.called;

                done();
            });
        });
    });

    describe('POST /usuario/entrar', () => {
        let sandbox = sinon.createSandbox();
        let entrarIvk = sandbox.stub();
        let iniciarSessaoIvk = sandbox.stub();

        let router = proxyquire('../lib/routing/routes.js', {
            '../model/usuario': {
                entrar: entrarIvk
            },
            '../service/autenticacao': {
                iniciarSessao: iniciarSessaoIvk
            }
        });

        afterEach(() => {
            sandbox.reset();
        });

        it('Should forward BadRequestError if content type is wrong.', done => {
            let req = {
                method: 'POST',
                url: '/usuario/entrar',
                headers: {'content-type': 'text/plain'}
            };
            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(BadRequestError);
                expect(erro.message).to.be.equals('Tipo de conteúdo inválido.');
                done();
            });
        });

        it('Should forward BadRequestError if json is malformed.', done => {
            let req = {
                method: 'POST',
                url: '/usuario/entrar',
                headers: {'content-type': 'application/json'},
                get body() { throw new Error('thrown on purpose'); }
            };
            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(BadRequestError);
                expect(erro.message).to.be.equals('Json inválido.');
                done();
            });
        });

        it('Should forward ConflictError if usuario throws ErroNegocio.', done => {
            let req = {
                method: 'POST',
                url: '/usuario/entrar',
                headers: {'content-type': 'application/json'},
                body: { }
            };

            entrarIvk.returns(new Promise(() => {
                throw new ErroNegocio('thrown on purpose');
            }));

            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(ConflictError);
                expect(erro.message).to.be.equals('thrown on purpose');
                done();
            });
        });

        it('Should forward ConflictError if usuario rejects with ErroNegocio.', done => {
            let req = {
                method: 'POST',
                url: '/usuario/entrar',
                headers: {'content-type': 'application/json'},
                body: { }
            };

            entrarIvk.returns(Promise.reject(
                new ErroNegocio('thrown on purpose')));

            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(ConflictError);
                expect(erro.message).to.be.equals('thrown on purpose');
                done();
            });
        });

        it('Should forward InternalServerError if usuario throws Error.', done => {
            let req = {
                method: 'POST',
                url: '/usuario/entrar',
                headers: {'content-type': 'application/json'},
                body: { }
            };

            entrarIvk.returns(Promise.reject(
                new ErroNegocio('thrown on purpose')));

            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(ConflictError);
                expect(erro.message).to.be.equals('thrown on purpose');
                done();
            });
        });

        it('Should forward InternalServerError if usuario rejects with Error', done => {
            let req = {
                method: 'POST',
                url: '/usuario/entrar',
                headers: {'content-type': 'application/json'},
                body: { }
            };

            entrarIvk.returns(Promise.reject(new Error('thrown on purpose')));

            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(InternalServerError);
                expect(erro.message).to.be.equals('Erro interno do servidor.');
                done();
            });
        });

        it('Should forward ConflictError if autenticacao rejects with ErroNegocio.', done => {
            let usuario = {};
            let credenciais = {};
            let req = {
                method: 'POST',
                url: '/usuario/entrar',
                headers: {'content-type': 'application/json'},
                body: credenciais
            };

            entrarIvk.returns(Promise.resolve(usuario));
            iniciarSessaoIvk.returns(Promise.reject(
                new ErroNegocio('thrown on purpose')));

            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(ConflictError);
                expect(erro.message).to.be.equals('thrown on purpose');
                done();
            });
        });

        it('Should forward InternalServerError if autenticacao rejects with Error.', done => {
            let usuario = {};
            let credenciais = {};
            let req = {
                method: 'POST',
                url: '/usuario/entrar',
                headers: {'content-type': 'application/json'},
                body: credenciais
            };

            entrarIvk.returns(Promise.resolve(usuario));
            iniciarSessaoIvk.returns(Promise.reject(
                new Error('thrown on purpose')));

            router(req, {}, erro => {
                expect(erro).to.be.instanceOf(InternalServerError);
                expect(erro.message).to.be.equals('Erro interno do servidor.');
                done();
            });
        });

        it('Should respond with user from database.', done => {
            let statusIvk = sandbox.stub();
            let jsonIvk = sandbox.stub();

            let usuario = {};
            let credenciais = {};
            let req = {
                method: 'POST',
                url: '/usuario/entrar',
                headers: {'content-type': 'application/json'},
                body: credenciais
            };

            entrarIvk.returns(Promise.resolve(usuario));
            iniciarSessaoIvk.returns(Promise.resolve());

            let res = {status: statusIvk, json: jsonIvk, end: function() {
                expect(statusIvk).to.be.calledOnce.and.to.be.calledWith(200);
                expect(jsonIvk).to.be.calledOnce.and.to.be.calledWith(usuario);
                done();
            }};

            router(req, res, done);
        });
    });
});