const SERVER_PORT = process.env.SERVER_PORT || 9001;
const express = require('express');
const winston = require('winston');

const app = express();

const router = require('./lib/routing/routes');
const errorRoutes = require('./lib/routing/error-routes');

app.use(require('body-parser').json());

app.use('/api', router);
app.use(errorRoutes);

app.listen(SERVER_PORT, () => {
    winston.info(`Escutando na porta ${SERVER_PORT}.`);
});

