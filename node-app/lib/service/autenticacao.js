const DURACAO_SESSAO = parseInt(process.env.DURACAO_SESSAO_EM_SEGUNDOS || 180);
const moment = require('moment');
const temposSessoes = {};

function tokenExpirado(token) {
    let tempo = temposSessoes[token];
    if (!tempo)
        return true;
    return moment().isAfter(tempo);
}

function iniciarSessao(token) {
    temposSessoes[token] = moment().add(DURACAO_SESSAO, 'seconds');
    return Promise.resolve();
}

function sessaoValida(token) {
    if (!token)
        return Promise.resolve(false);
    return Promise.resolve(!tokenExpirado(token));
}

function obterToken(headers) {
    if (!headers)
        return null;
    let authHeader = headers['authorization'];
    if (!authHeader)
        return null;
    let match = /Bearer (.*)/.exec(authHeader);
    if (!match)
        return null;
    return match[1];
}

module.exports = {iniciarSessao, sessaoValida, obterToken};