const MONGO_SERVER = process.env.MONGO_SERVER || 'localhost';
const MONGO_PORT = parseInt(process.env.MONGO_PORT || 27017);

const {MongoClient, ObjectId} = require('mongodb');

const DB_URL = `mongodb://${MONGO_SERVER}:${MONGO_PORT}/`;

function abrirConexao() {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, function(e, conn) {
            if (e) return reject(e);
            resolve({connection: conn, db:conn.db('db_usuarios')});
        });
    });
}

function salvarUsuario(db, usuario) {
    return new Promise((resolve, reject) => {
        db.collection('usuarios').insertOne(usuario, (e, res) => {
            if (e) return reject(e);
            resolve(res.insertedId);
        });
    });
}

function rodarConsulta(db, consulta) {
    return new Promise(resolve => {
        resolve(db.collection('usuarios').findOne(consulta));
    });
}

function inserir(usuario) {
    return abrirConexao().then(({connection, db}) => {
        return salvarUsuario(db, usuario).then(id => {
            connection.close();
            return Promise.resolve(id);
        });
    });
}

function buscarUsuarioPorEmail(email) {
    return abrirConexao().then(({connection, db}) => {
        return rodarConsulta(db, {email}).then(usuario => {
            connection.close();
            return Promise.resolve(usuario);
        });
    });
}

function buscarUsuarioPorId(id) {
    return abrirConexao().then(({connection, db}) => {

        // Linha desabilitada - A API do MongoDB não é conformante com
        // os padrões de linting adotados no projeto.
        // eslint-disable-next-line new-cap
        let _id = ObjectId(id);

        return rodarConsulta(db, {_id}).then(usuario => {
            connection.close();
            return Promise.resolve(usuario);
        });
    });
}

function existeUsuarioComEmail(email) {
    return buscarUsuarioPorEmail(email).then(usuario => {
        return Promise.resolve(undefined !== usuario && null !== usuario);
    });
}

module.exports = {
    inserir,
    existeUsuarioComEmail,
    buscarUsuarioPorEmail,
    buscarUsuarioPorId
};