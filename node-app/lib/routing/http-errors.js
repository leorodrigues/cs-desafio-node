
class BadRequestError extends Error {
    constructor(message) {
        super(message || 'Request inválido.');
        this.statusCode = 400;
    }
}

class InternalServerError extends Error {
    constructor(message) {
        super(message || 'Erro interno do servidor.');
        this.statusCode = 500;
    }
}

class ConflictError extends Error {
    constructor(message) {
        super(message || 'Conflito.');
        this.statusCode = 409;
    }
}

class AccessDeniedError extends Error {
    constructor(message) {
        super(message || 'Acesso negado.');
        this.statusCode = 403;
    }
}

class UnauthorizedError extends Error {
    constructor(message) {
        super(message || 'Não autorizado.');
        this.statusCode = 401;
    }
}

module.exports = {
    AccessDeniedError,
    UnauthorizedError,
    ConflictError,
    BadRequestError,
    InternalServerError
};