
function notFoundHandler(request, response) {
    response.status(404);
    response.json({mensagem: 'Não encontrado'});
    response.end();
}

// Linha desabilitada - A API do express exige que o middleware de tratamento
// de erros tenha quatro parâmetros, mesmo que um deles não seja usado.
// eslint-disable-next-line no-unused-vars
function exceptionHandler(error, request, response, next) {
    response.status(error.statusCode || 500);
    response.json({mensagem: error.message});
    response.end();
}

module.exports = [exceptionHandler, notFoundHandler];