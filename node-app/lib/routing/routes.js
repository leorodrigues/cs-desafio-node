const express = require('express');
const winston = require('winston');

const {ErroNegocio} = require('../model/erros-negocio');
const usuario = require('../model/usuario');
const autenticacao = require('../service/autenticacao');
const database = require('../service/database');

const {
    UnauthorizedError,
    ConflictError,
    BadRequestError,
    InternalServerError
} = require('./http-errors');

const router = new express.Router();

function isJsonRequest(request) {
    return request.headers['content-type'] === 'application/json';
}

router.post('*', (request, response, next) => {
    if (!isJsonRequest(request))
        throw new BadRequestError('Tipo de conteúdo inválido.');
    next();
});

router.post('/usuario', (request, response, next) => {

    try {
        var input = request.body;
    } catch(erro) {
        winston.error('Erro desserializando novo usuário.', erro.message);
        throw new BadRequestError('Json inválido.');
    }

    let handleSuccess = novoUsuario => {
        response.status(200);
        response.json(novoUsuario);
        response.end();
    };

    let handleError = erro => {
        winston.error('Erro inserindo novo usuário.', erro.message);
        if (erro instanceof ErroNegocio)
            next(new ConflictError(erro.message));
        else
            next(new InternalServerError());
    };

    usuario.novo(input)
        .then(handleSuccess, handleError)
        .catch(handleError);
});

router.post('/usuario/entrar', (request, response, next) => {
    
    try {
        var input = request.body;
    } catch(erro) {
        winston.error('Erro desserializando credenciais', erro.message);
        throw new BadRequestError('Json inválido.');
    }

    let encaminharErro = erro => {
        winston.error('Erro identificando usuário.', erro.message);
        if (erro instanceof ErroNegocio)
            next(new ConflictError(erro.message));
        else
            next(new InternalServerError());
    };

    let resolverEntradaUsuario = usuario => {

        let responderComUsuario = () => {
            response.status(200);
            response.json(usuario);
            response.end();
        };

        return autenticacao.iniciarSessao(usuario.token)
            .then(responderComUsuario, encaminharErro);
    };

    usuario.entrar(input)
        .then(resolverEntradaUsuario, encaminharErro)
        .catch(encaminharErro);
});

router.get('/usuario/:id', (request, response, next) => {

    let token = autenticacao.obterToken(request.headers);

    if (!token)
        throw new UnauthorizedError();

    let encaminharErro = error => {
        winston.error('Erro buscando usuário por ID.', error.message);
        next(new InternalServerError());
    };

    let resolverBuscaPorId = usuario => {
        let resolverValidacaoSessao = valido => {
            if (!valido) {
                next(new UnauthorizedError('Sessão inválida.'));
            } else {
                response.status(200);
                response.json(usuario);
                response.end();
            }
        };

        if (!usuario) {
            next();
        } else if (usuario.token !== token) {
            next(new UnauthorizedError());
        } else {
            return autenticacao.sessaoValida(token)
                .then(resolverValidacaoSessao, encaminharErro);
        }
    };

    database.buscarUsuarioPorId(request.params.id)
        .then(resolverBuscaPorId, encaminharErro)
        .catch(encaminharErro);
});

module.exports = router;