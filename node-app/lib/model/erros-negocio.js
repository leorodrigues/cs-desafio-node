
class ErroNegocio extends Error {
    constructor(mensagem) {
        super(mensagem);
    }
}

class ErroCredencialInvalida extends ErroNegocio {
    constructor(mensagem) {
        super(mensagem);
    }
}

module.exports = {ErroNegocio, ErroCredencialInvalida};