const database = require('../service/database');
const moment = require('moment');
const guid = require('guid');

const {ErroNegocio, ErroCredencialInvalida} = require('./erros-negocio');

function entrar(input) {
    return new Promise((resolve, reject) => {

        let handleBuscaPorEmail = usuario => {
            if (usuario && usuario.senha === input.senha) {
                resolve(usuario);
            } else {
                reject(new ErroCredencialInvalida(
                    'Usuário e/ou senha inválidos.'));
            }
        };

        return database.buscarUsuarioPorEmail(input.email)
            .then(handleBuscaPorEmail, reject);
    });
}

function novo(input) {
    return new Promise((resolve, reject) => {

        let handleBuscaPorEmail = resultado => {
            if (resultado) {
                reject(new ErroNegocio('E-mail já existente.'));
                return;
            }

            let agora = moment().valueOf();
            let usuario = {
                'nome': input.nome,
                'email': input.email,
                'senha': input.senha,
                'telefones': input.telefones,
                'data_criacao': agora,
                'data_atualizacao': agora,
                'ultimo_login': agora,
                'token': guid.raw()
            };

            return database.inserir(usuario).then(resolve, reject);
        };

        return database.existeUsuarioComEmail(input.email)
            .then(handleBuscaPorEmail, reject);
    });
}

module.exports = {novo, entrar};