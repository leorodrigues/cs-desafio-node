# EXECUÇÃO

## Estrutura do projeto

Na raiz deste projeto, você encontra a pasta "node-app" com a aplicação node
que "responde" ao desafio. Ao lado dela estão:
- docker-compose.yml: Arquivo para execução usando docker-compose
- mongo-data: Diretório de volume para a imagem de docker do mongodb. Este
diretório conterá os arquivos de dados do mongodb preservando o estado da base
de dados entre as sessões do docker.

## USANDO DOCKER

1. Instalar o docker e docker-composer executando as operações descritas
nos links abaixo:
 - Install docker
 - Install docker-composer

2. Com o docker composer já funcionando, execute os comandos abaixo a partir
do caminho clonado:

Rodar num terminal:
```bash
$ docker-compose up
```

Rodar em outra instância de terminal:
```bash
$ docker exec -it leorodriguesrj-sign-in-api npm run init-db
```

Este último comando vai inicializar a base de dados do mongodb. Usando dois
terminais pode ser mais interessante pois permite acompanhar o log da aplicação
e do mongodb conforme os endpoints são ativados, mas o docker-compose aceita a
opção "-d" que roda com o terminal "detached" e não prende a console,
permitindo o uso de apenas um terminal.

### Rodando docker sob um proxy

Pode ser necessário fornecer configurações de proxy para serem usadas no passo
de build do container do docker. Para tanto crie o arquivo ```.env``` ao lado
de ```docker-compose.yml``` conforme abaixo:

```bash
$ touch .env
$ vi .env
```

Em seguida, adicione as linhas abaixo, substituindo as informações conforme
o seu ambiente operacional:

```
npm_config_http_proxy=http://[USERNAME:USERPASS@]PROXY_HOST:PROXY_PORT
npm_config_https_proxy=http://[USERNAME:USERPASS@]PROXY_HOST:PROXY_PORT
npm_config_strict-ssl=false
```

### Endpoints

Em caso de execução via docker, a api fica exposta na porta 8080 e os endpoints
ficam assim:

- Sign-in: POST http://localhost:8080/api/usuario/entrar
- Cadastro: POST http://localhost:8080/api/usuario
- Consulta: GET http://localhost:8080/api/usuario/:id

## USANDO NODE DIRETAMENTE

1. Certifique-se de que tem o mongodb e node 8.x instalados. O mongodb deve
estar executando.
2. A partir da raiz do projeto rode os seguintes comandos:

```bash
$ cd node-app
$ npm install
$ npm run init-db
$ npm start
```

3. Opcionalmente, o projeto pode ser executado com debugger ativado, neste caso,
use ```npm run debug``` no lugar de ```npm start```. O comando 
```npm run lint``` está disponível para verificação de conformidade do código
fonte.

### Endpoints

Em caso de execução via , a api fica exposta na porta 9001 e os endpoints
ficam assim:

- Sign-in: POST http://localhost:9001/api/usuario/entrar
- Cadastro: POST http://localhost:9001/api/usuario
- Consulta: GET http://localhost:9001/api/usuario/:id

## USANDO ENDPOINTS DA AMAZON

O projeto está hospedado numa máquina Amazon e os endpoints estão disponíveis
abaixo entre os dias 24/01/2018 e 31/01/2018. Para acessos após este período,
por favor solicite reativação do sistema à
Leonardo Teixeira <leorodriguesrj@gmail.com>.

Host: http://ec2-54-213-12-138.us-west-2.compute.amazonaws.com:8080
URIs:
- Sign-in: POST /api/usuario/entrar
- Cadastro: POST /api/usuario/
- Consulta: GET /api/usuario/:id